export interface BulkCardProps {
    id: number;
    name: string;
    description?: string;
    items: RepoBranchesProps[] | [];
}

export interface RepoBranchesProps {
    id: string | number;
    name: string;
    git?: string;
    ispublic?: boolean,
    branch: string;
    speed?: number | string;
    reability?: number | string;
    builds?: number | string;
    stats: RepoBranchesStatsProps[];
    joblist: JobProps[];
}

export interface RepoBranchesStatsProps {
    id: string | number;
    live?: boolean;
    value?: number;
}

export interface JobProps {
    id: string | number;
    name?: string;
    auther?: string;
    build?: string;
    buildfile?: string;
    time?: string;
}