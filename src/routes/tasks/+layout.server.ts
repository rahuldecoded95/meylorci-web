import type { BulkCardProps } from "$lib/types";
import type { LayoutServerLoad, PageServerLoad } from "../$types";

const apires: BulkCardProps = {
    id: 1,
    name: 'Test 1',
    description: 'Test 1 description',
    items: [
        {
            id: 1,
            name: 'bluck',
            git: 'git@gitlab.com:Aaronorosen/meylorci-web.git',
            ispublic: true,
            branch: 'main',
            speed: 68.0,
            reability: 17,
            builds: 26138,
            stats: [
                {
                    id: 1,
                    live: true,
                    value: 80
                },
                {
                    id: 2,
                    value: 80
                },
                {
                    id: 3,
                    live: true,
                    value: 80
                },
                {
                    id: 4,
                    live: true,
                    value: 100
                },
                {
                    id: 5,
                    live: true,
                    value: 100
                },
                {
                    id: 6,
                    live: true,
                    value: 100
                },
                {
                    id: 7,
                    live: true,
                    value: 80
                },
                {
                    id: 8,
                    live: true,
                    value: 80
                },
                {
                    id: 9,
                    live: true,
                    value: 80
                },
                {
                    id: 10,
                    live: true,
                    value: 100
                },
                {
                    id: 11,
                    live: false,
                    value: 80
                },
                {
                    id: 12,
                    live: true,
                    value: 80
                },
                {
                    id: 13,
                    value: 80
                },
                {
                    id: 14,
                    live: true,
                    value: 100
                },
                {
                    id: 15,
                    value: 80
                },
                {
                    id: 16,
                    value: 80
                },
                {
                    id: 17,
                    value: 80
                },
                {
                    id: 18,
                    value: 80
                },
                {
                    id: 19,
                    live: false,
                    value: 80
                },
                {
                    id: 20,
                    live: true,
                    value: 100
                },
                {
                    id: 21,
                    live: false,
                    value: 80
                },
                {
                    id: 22,
                    live: true,
                    value: 100
                },
                {
                    id: 23,
                    live: false,
                    value: 100
                },
                {
                    id: 23,
                    live: false,
                    value: 80
                },
                // create more
                {
                    id: 24,
                    live: true
                },
                {
                    id: 25,
                    live: true
                },
                {
                    id: 26,
                    live: true,
                    value: 100
                },
                {
                    id: 27,
                    value: 80
                },
                {
                    id: 28,
                    value: 80
                },
                {
                    id: 29,
                    value: 80
                },
                {
                    id: 30,
                    value: 80
                }
            ],
            joblist: [
                {
                    id: 1,
                    name: 'state-Co-command-get_redin_cvs_files',
                    auther: 'KalanaKt',
                    build: '#159876',
                    buildfile: './script/batch.sh',
                    time: 'Thu 7th Sepat 5.41 PM'
                },
                {
                    id: 2,
                    name: 'state-Co-command-get_redin_cvs_files',
                    auther: 'KalanaKt',
                    build: '#159876',
                    buildfile: './script/batch.sh',
                    time: 'Thu 7th Sepat 5.41 PM'
                },
                {
                    id: 3,
                    name: 'state-Co-command-get_redin_cvs_files',
                    auther: 'KalanaKt',
                    build: '#159876',
                    buildfile: './script/batch.sh',
                    time: 'Thu 7th Sepat 5.41 PM'
                },
                {
                    id: 4,
                    name: 'state-Co-command-get_redin_cvs_files',
                    auther: 'KalanaKt',
                    build: '#159876',
                    buildfile: './script/batch.sh',
                    time: 'Thu 7th Sepat 5.41 PM'
                }
            ]
        },
        {
            id: 2,
            name: 'bluck 2',
            git: '',
            ispublic: true,
            branch: 'main',
            speed: 68.05,
            reability: 17,
            builds: 26138,
            stats: [
                {
                    id: 1,
                    live: true,
                    value: 80
                },
                {
                    id: 2,
                    live: true,
                    value: 80
                },
                {
                    id: 3,
                    live: true,
                    value: 80
                },
                {
                    id: 4,
                    live: true,
                    value: 100
                },
                {
                    id: 5,
                    live: true,
                    value: 100
                },
                {
                    id: 6,
                    live: true,
                    value: 100
                },
                {
                    id: 7,
                    live: true,
                    value: 80
                },
                {
                    id: 8,
                    value: 80
                },
                {
                    id: 9,
                    value: 80
                },
                {
                    id: 10,
                    value: 80
                },
                {
                    id: 11,
                    live: false,
                    value: 80
                },
                {
                    id: 12,
                    live: true,
                    value: 80
                },
                {
                    id: 13,
                    live: false,
                    value: 80
                },
                {
                    id: 14,
                    live: true,
                    value: 100
                },
                {
                    id: 15,
                    live: false,
                    value: 100
                },
                {
                    id: 16,
                    live: true,
                    value: 100
                },
                {
                    id: 17,
                    live: false,
                    value: 80
                },
                {
                    id: 18,
                    live: true,
                    value: 80
                },
                {
                    id: 19,
                    live: true,
                    value: 80
                },
                {
                    id: 20,
                    live: true,
                    value: 100
                },
                {
                    id: 21,
                    live: true,
                    value: 80
                },
                {
                    id: 22,
                    live: true,
                    value: 100
                },
                {
                    id: 23,
                    live: true,
                    value: 100
                },
                {
                    id: 23,
                    live: false,
                    value: 80
                },
                {
                    id: 24,
                    live: false
                },
                {
                    id: 25,
                    live: false
                },
                {
                    id: 26
                },
                {
                    id: 27,
                    live: true,
                    value: 100
                },
                {
                    id: 28,
                    live: true,
                    value: 100
                },
                {
                    id: 29,
                    live: true,
                    value: 100
                },
                {
                    id: 30
                }
            ],
            joblist: [
                {
                    id: 1,
                    name: 'state-Co-command-get_redin_cvs_files',
                    auther: 'KalanaKt',
                    build: '#159876',
                    buildfile: './script/batch.sh',
                    time: 'Thu 7th Sepat 5.41 PM'
                },
                {
                    id: 2,
                    name: 'state-Co-command-get_redin_cvs_files',
                    auther: 'KalanaKt',
                    build: '#159876',
                    buildfile: './script/batch.sh',
                    time: 'Thu 7th Sepat 5.41 PM'
                },
                {
                    id: 3,
                    name: 'state-Co-command-get_redin_cvs_files',
                    auther: 'KalanaKt',
                    build: '#159876',
                    buildfile: './script/batch.sh',
                    time: 'Thu 7th Sepat 5.41 PM'
                },
                {
                    id: 4,
                    name: 'state-Co-command-get_redin_cvs_files',
                    auther: 'KalanaKt',
                    build: '#159876',
                    buildfile: './script/batch.sh',
                    time: 'Thu 7th Sepat 5.41 PM'
                }
            ]
        },
        {
            id: 3,
            name: 'bluck 3',
            git: '',
            ispublic: false,
            branch: 'main',
            speed: 33.7,
            reability: 17,
            builds: 26138,
            stats: [
                {
                    id: 1,
                    live: true,
                    value: 100
                },
                {
                    id: 2,
                    live: true,
                    value: 80
                },
                {
                    id: 3,
                    live: true,
                    value: 80
                },
                {
                    id: 4,
                    live: true,
                    value: 100
                },
                {
                    id: 5,
                    live: true,
                    value: 100
                },
                {
                    id: 6
                },
                {
                    id: 7
                },
                {
                    id: 8,
                    live: true,
                    value: 80
                },
                {
                    id: 9,
                    live: true,
                    value: 80
                },
                {
                    id: 10,
                    live: true,
                    value: 100
                },
                {
                    id: 11,
                    live: true,
                    value: 80
                },
                {
                    id: 12,
                    live: true,
                    value: 80
                },
                {
                    id: 13,
                    live: true,
                    value: 80
                },
                {
                    id: 14
                },
                {
                    id: 15,
                    live: true,
                    value: 100
                },
                {
                    id: 16,
                    live: true,
                    value: 100
                },
                {
                    id: 17,
                    live: false,
                    value: 80
                },
                {
                    id: 18,
                    live: true,
                    value: 80
                },
                {
                    id: 19,
                    live: false,
                    value: 80
                },
                {
                    id: 20,
                    live: true,
                    value: 100
                },
                {
                    id: 21,
                    live: false,
                    value: 80
                },
                {
                    id: 22,
                    live: true,
                    value: 100
                },
                {
                    id: 23,
                    live: false,
                    value: 100
                },
                {
                    id: 23,
                    live: false,
                    value: 80
                },
                {
                    id: 24,
                    live: false
                },
                {
                    id: 25,
                    live: false
                },
                {
                    id: 26,
                    live: true,
                    value: 100
                },
                {
                    id: 27
                },
                {
                    id: 28,
                    live: true,
                    value: 100
                },
                {
                    id: 29,
                    live: true,
                    value: 100
                },
                {
                    id: 30,
                    live: true,
                    value: 100
                }
            ],
            joblist: [
                {
                    id: 1,
                    name: 'state-Co-command-get_redin_cvs_files',
                    auther: 'KalanaKt',
                    build: '#159876',
                    buildfile: './script/batch.sh',
                    time: 'Thu 7th Sepat 5.41 PM'
                },
                {
                    id: 2,
                    name: 'state-Co-command-get_redin_cvs_files',
                    auther: 'KalanaKt',
                    build: '#159876',
                    buildfile: './script/batch.sh',
                    time: 'Thu 7th Sepat 5.41 PM'
                },
                {
                    id: 3,
                    name: 'state-Co-command-get_redin_cvs_files',
                    auther: 'KalanaKt',
                    build: '#159876',
                    buildfile: './script/batch.sh',
                    time: 'Thu 7th Sepat 5.41 PM'
                },
                {
                    id: 4,
                    name: 'state-Co-command-get_redin_cvs_files',
                    auther: 'KalanaKt',
                    build: '#159876',
                    buildfile: './script/batch.sh',
                    time: 'Thu 7th Sepat 5.41 PM'
                }
            ]
        }
    ]
};

export const load: LayoutServerLoad = async () => {
    return {
        data: apires
    };
}